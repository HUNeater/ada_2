with Ada.Text_IO; use Ada.Text_IO;
with Ada.Calendar; use Ada.Calendar;

procedure elevator_control is
	type Commands is (Forward, Backward, Power_Off);
	type Speed is range -1 .. 1;
	MOTOR_BURNED_OUT : exception;

	task Elevator is
		entry Move_Up;
		entry Move_Down;
	end Elevator;

	task Motor is
		entry Command(d : Commands);
	end Motor;

	task body Motor is 
		tDelta, tForwards, tBackwards : Duration := 0.0;
		tCurrent, tLast : Time := Clock;
		v : Speed := 0;
		s : Duration := 0.0;
		keepRunning : Boolean := true;
	begin
		while keepRunning loop 
			select
				accept Command(d : Commands) do
					begin
						if d = Forward then
							v := Speed'succ(v);
						elsif d = Backward then
							v := Speed'pred(v);
						else
							keepRunning := false;
						end if;
					exception
						when constraint_error => null;
					end;	
				end Command;
			else
				--delay 0.1; 

				tCurrent := Clock;
				tDelta := tCurrent - tLast;
				tLast := tCurrent;
					
				if v = 1 then
					tForwards := tForwards + tDelta;
				elsif v = -1 then
					tBackwards := tBackwards + tDelta;
				end if;
					
				s := tForwards - tBackwards;

				if s > 0.1 then
					select
						Elevator.Move_Up;
					or
						delay 0.1;
						raise MOTOR_BURNED_OUT;
					end select;
					
					tForwards := 0.0;
					tBackwards := 0.0;
				elsif s < -0.1 then
					select 
						Elevator.Move_Down;	
					or
						delay 0.1;
						raise MOTOR_BURNED_OUT;
					end select;
					
					tForwards := 0.0;
					tBackwards := 0.0;
				end if;
			end select;
		end loop;
	exception
		when MOTOR_BURNED_OUT => Put_Line("Motor burned out!");
	end Motor;

	task body Elevator is
		currentPos : Natural := 0;       
	begin
		loop
			select when currentPos < 40 =>
				accept Move_Up do
					currentPos := currentPos + 1;
					Put_Line("Current pos: " &Natural'image(currentPos));
				end Move_Up;
			or when currentPos > 0 =>
				accept Move_Down do
					currentPos := currentPos - 1;
					Put_Line("Current pos: " &Natural'image(currentPos));
				end Move_Down;
			or
				terminate;
			end select;	
		end loop;
	end Elevator;
begin
	Motor.Command(Forward);
end elevator_control;
