with Ada.Text_IO; use Ada.Text_IO;
with Ada.Calendar; use Ada.Calendar;

procedure elevator_control is
	type Commands is (Forward, Backward, Power_Off);

	task Elevator is
		entry Move_Up;
		entry Move_Down;
	end Elevator;

	task Motor is
		entry Command(d : Commands);
	end Motor;

	task Controller is
		entry sensor(level : Natural);
		entry Request(level : Natural);
	end Controller;

	task type Signal(level : Natural);

	task body Signal is begin
		Controller.sensor(level);
	end Signal;

	task body Controller is
		currentLevel : Natural := 0;	
		targetLevel : Integer := -1;
		bHasJob : Boolean := false;
		currentStatus : Commands := Power_Off;
	begin
		while Motor'callable loop 
			select
				accept sensor(level : Natural) do
					currentLevel := level;
					Put_Line("Elevator is at floor " & Natural'image(level));

					if currentLevel = targetLevel then
						bHasJob := false;

						if currentStatus = Forward then
							Motor.Command(Backward);
						elsif currentStatus = Backward then
							Motor.Command(Forward);
						end if;

						delay 1.0;
					end if;
				end sensor;
			or when not bHasJob =>
				accept Request(level : Natural) do
					targetLevel := level;
				end Request;

				bHasJob := true;
				if targetLevel > currentLevel then
					currentStatus := Forward;
					Motor.Command(currentStatus);
				elsif targetLevel < currentLevel then
					currentStatus := Backward;
					Motor.Command(currentStatus);
				else
					bHasJob := false;
				end if;
			or
				terminate;
			end select;
		end loop;
	end Controller;

	task body Motor is 
		MOTOR_BURNED_OUT : exception;
		type Speed is range -1 .. 1;
		tDelta, tForwards, tBackwards : Duration := 0.0;
		tCurrent, tLast : Time := Clock;
		v : Speed := 0;
		s : Duration := 0.0;
		keepRunning : Boolean := true;
	begin
		while keepRunning loop 
			select
				accept Command(d : Commands) do
					begin
						if d = Forward then
							v := Speed'succ(v);
						elsif d = Backward then
							v := Speed'pred(v);
						elsif d = Power_Off then
							keepRunning := false;
						end if;
					exception
						when constraint_error => null;
					end;
				end Command;
			else	
				tCurrent := Clock;
				tDelta := tCurrent - tLast;
				tLast := tCurrent;
					
				if v = 1 then
					tForwards := tForwards + tDelta;
				elsif v = -1 then
					tBackwards := tBackwards + tDelta;
				end if;
					
				s := tForwards - tBackwards;

				if s > 0.1 and v = 1 then
					select
						Elevator.Move_Up;
					or
						delay 0.1;
						raise MOTOR_BURNED_OUT;
					end select;
					
					tForwards := 0.0;
					tBackwards := 0.0;
				elsif s < -0.1 and v = -1 then
					select 
						Elevator.Move_Down;		
					or
						delay 0.1;	
						raise MOTOR_BURNED_OUT;
					end select;
					
					tForwards := 0.0;
					tBackwards := 0.0;
				end if;
			end select;
		end loop;
	exception
		when MOTOR_BURNED_OUT => Put_Line("Motor burned out!");
	end Motor;

	task body Elevator is
		currentPos : Natural := 0;
	   	s : access Signal;	
	begin
		while Motor'callable loop 
			select when currentPos < 40 =>
				accept Move_Up do
					currentPos := currentPos + 1;
					Put_Line("Position: " & Natural'image(currentPos));
				end Move_Up;
	
				if currentPos rem 10 < 2 then
					s := new Signal(currentPos / 10);
				elsif currentPos rem 10 = 9 then
					s := new Signal((currentPos + 9 ) / 10);
				end if;
			or when currentPos > 0 =>
				accept Move_Down do
					currentPos := currentPos - 1;
					Put_Line("Position: " & Natural'image(currentPos));
				end Move_Down;
			
				if currentPos rem 10 < 2 then
					s := new Signal(currentPos / 10);
				elsif currentPos rem 10 = 9 then
					s := new Signal((currentPos + 9 ) / 10);
				end if;
			or
				terminate;
			end select;
		end loop;
	end Elevator;

	line : String(1..1);
	last : Natural;
	v : Integer;
begin
	begin
		while Motor'callable loop
			Get_Line(line, last);
			v := Integer'value(line);
			if v >= 0 and then v < 5 then
			       Controller.Request(v);
			else
		 		Put_Line("Wrong input!");
			end if;		
		end loop;
	exception
		when others => Motor.Command(Power_Off);
	end;
end elevator_control;
