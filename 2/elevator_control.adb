with Ada.Text_IO; use Ada.Text_IO;
with Ada.Calendar; use Ada.Calendar;

procedure elevator_control is
	type Direction is (Forward, Backward);
	type Speed is range -1 .. 1;

	task Motor is
		entry Command(d : Direction);
	end Motor;

	task body Motor is 
		tDelta, tForward, tBackwards : Duration := 0.0;
		tCurrent, tLast : Time := Clock;
		v : Speed := 0;
	begin
		loop 
			select
				accept Command(d : Direction) do
					begin
						if d = Forward then
							v := Speed'succ(v);
						else
							v := Speed'pred(v);
						end if;
					exception
						when constraint_error => null;
					end;	
				end Command;
			or
				terminate;
			end select;

			tCurrent := Clock;
			tDelta := tCurrent - tLast;
			tLast := tCurrent;
				
			if v = 1 then
				tForward := tForward + tDelta;
			elsif v = -1 then
				tBackwards := tBackwards + tDelta;
			end if;
		end loop;	
	end Motor;

	task Elevator is
		entry Move_Up;
		entry Move_Down;
	end Elevator;

	task body Elevator is
		currentPos : Natural := 0;       
	begin
		loop
			select when currentPos < 40 =>
				accept Move_Up do
					currentPos := currentPos + 1;
					Put_Line("Current pos: " &Natural'image(currentPos));
				end Move_Up;
			or when currentPos > 0 =>
				accept Move_Down do
					currentPos := currentPos - 1;
					Put_Line("Current pos: " &Natural'image(currentPos));
				end Move_Down;
			or
				terminate;
			end select;	
		end loop;
	end Elevator;
begin
	for i in 0..50 loop
		select
			Elevator.Move_Up;
		or delay 0.0;
		end select;
	end loop;

	for i in 0..50 loop
		select
			Elevator.Move_Down;
		or delay 0.0;
		end select;
	end loop;
end elevator_control;
