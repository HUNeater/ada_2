with Ada.Text_IO; use Ada.Text_IO;
with Ada.Calendar; use Ada.Calendar;

procedure elevator_control is
	type Direction is (Forward, Backward);
	type Speed is range -1 .. 1;

	task Motor is
		entry Command(d : Direction);
	end Motor;

	task body Motor is 
		tDelta, tForward, tBackwards : Duration := 0.0;
		tCurrent, tLast : Time := Clock;
		v : Speed := 0;
	begin
		loop 
			select
				accept Command(d : Direction) do
					begin
						if d = Forward then
							v := Speed'succ(v);
						else
							v := Speed'pred(v);
						end if;
					exception
						when constraint_error => null;
					end;
					
					Put_Line("Moved so far: " & Duration'image(tForward - tBackwards));
					Put_Line("Speed set to: " & Speed'Image(v));
				end Command;
			or
				terminate;
			end select;

			tCurrent := Clock;
			tDelta := tCurrent - tLast;
			tLast := tCurrent;
				
			if v = 1 then
				tForward := tForward + tDelta;
			elsif v = -1 then
				tBackwards := tBackwards + tDelta;
			end if;
		end loop;	
	end Motor;
begin
	Motor.Command(Forward);
	delay 1.5;
	Motor.Command(Forward);
	delay 0.5;
	Motor.Command(Backward);
	delay 0.7;
	Motor.Command(Backward);
	delay 0.5;
	Motor.Command(Backward);
	delay 2.5;
	Motor.Command(Forward);
	delay 1.0;
	Motor.Command(Forward);
	delay 2.0;
end elevator_control;
