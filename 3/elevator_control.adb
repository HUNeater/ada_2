with Ada.Text_IO; use Ada.Text_IO;
with Ada.Calendar; use Ada.Calendar;

procedure elevator_control is
	type Direction is (Forward, Backward);
	type Speed is range -1 .. 1;

	task Elevator is
		entry Move_Up;
		entry Move_Down;
	end Elevator;

	task Motor is
		entry Command(d : Direction);
	end Motor;

	task body Motor is 
		tDelta, tForwards, tBackwards : Duration := 0.0;
		tCurrent, tLast : Time := Clock;
		v : Speed := 0;
		s : Duration := 0.0;
	begin
		loop 
			select
				accept Command(d : Direction) do
					begin
						if d = Forward then
							v := Speed'succ(v);
						else
							v := Speed'pred(v);
						end if;
					exception
						when constraint_error => null;
					end;	
				end Command;
			else
				--delay 0.1; 

				tCurrent := Clock;
				tDelta := tCurrent - tLast;
				tLast := tCurrent;
					
				if v = 1 then
					tForwards := tForwards + tDelta;
				elsif v = -1 then
					tBackwards := tBackwards + tDelta;
				end if;
					
				s := tForwards - tBackwards;

				if s > 0.1 then
					Elevator.Move_Up;
					tForwards := 0.0;
					tBackwards := 0.0;
				elsif s < -0.1 then
					Elevator.Move_Down;	
					tForwards := 0.0;
					tBackwards := 0.0;
				end if;
			end select;
		end loop;	
	end Motor;

	task body Elevator is
		currentPos : Natural := 0;       
	begin
		loop
			select when currentPos < 40 =>
				accept Move_Up do
					currentPos := currentPos + 1;
					Put_Line("Current pos: " &Natural'image(currentPos));
				end Move_Up;
			or when currentPos > 0 =>
				accept Move_Down do
					currentPos := currentPos - 1;
					Put_Line("Current Pos: " &Natural'image(currentPos));
				end Move_Down;
			or
				terminate;
			end select;	
		end loop;
	end Elevator;
begin
	delay 1.0;
	Put_Line("Forward");
	Motor.Command(Forward);
	delay 1.5;
	Motor.Command(Forward);
	delay 0.5;
	Put_Line("Stop");
	Motor.Command(Backward);
	delay 0.7;
	Put_Line("Backward");
	Motor.Command(Backward);
	delay 0.5;
	Motor.Command(Backward);
	delay 1.0;
	Put_Line("Stop");
	Motor.Command(Forward);
	delay 1.0;
	Put_Line("Forward");
	Motor.Command(Forward);
	delay 2.0;
	abort Motor;
end elevator_control;
